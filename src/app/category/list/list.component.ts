import { Component, OnInit } from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import { ToastrService } from 'ngx-toastr';
import { AppService } from 'src/app/app.service';
import { BehaviorService } from 'src/app/shared/behavior.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {


  filters: {page:any, count:any, search:any, role:any, isDeleted:any} = {page:1, count:10, search:'',role:'', isDeleted:false} 
  total = 0;
  pageSizeOptions = [5, 10, 25];
  showFirstLastButtons = true;
  data:any = [];
  id:any;
  deleteModal:any = false;

  constructor(private appService:AppService,
    private _bs:BehaviorService,
    private toast:ToastrService) { }

  ngOnInit(): void {
    this.getData()
  }


  search(){
    this.filters.page=1;
    this.getData()
  }

  clear(){
    this.filters.page=1;
    this.filters.search=''
    this.getData()
  }
  
  handlePageEvent(event: PageEvent) {
    // this.length = event.length;
    this.filters.count = event.pageSize;
    this.filters.page = event.pageIndex+1;
    console.log("page",event)
    this.getData()
  }

  getData(){
    this._bs.load(true)
    this.appService.getAll('common-category/all',this.filters).subscribe((res:any)=>{
      if(res.success){
        this.data = res.data.data
        this.total = res.data.total_count
        this._bs.load(false)
      }
    })
  }

  deleteF(id:any){
    this.id = id;
    this.deleteModal = true
  }

  delete(){
    let filter = {
      id:this.id
    }

    this._bs.load(true)
    this.appService.deleteRecord(filter,'common-category').subscribe((res:any)=>{
      if(res.success){
        this.toast.success(res.message)
        this.deleteModal = false;
        this.filters.page = 1
        this.getData()
      }
      
    })
  }

  statusChange(item:any){
    let status='active'
    let message='Do you want to Activate this category'
    if(item.status=='active'){
      status='deactive'
      message='Do you want to Deactivate this category'
    }
    if(window.confirm(message)){
      this._bs.load(true)
      this.appService.update({},`change/status?model=commoncategories&id=${item.id}&status=${status}`).subscribe(res=>{
        if(res.success){
          this.getData()
        }
        this._bs.load(false)
      })
    }
  }

}
