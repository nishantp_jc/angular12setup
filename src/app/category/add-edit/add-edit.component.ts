import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppService } from 'src/app/app.service';
import { BehaviorService } from 'src/app/shared/behavior.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class AddEditComponent implements OnInit {

  addEditForm:FormGroup
  id:any
  submitted:any
  host:any=environment.apiUrl
  constructor(private _fb:FormBuilder,private activeRoute:ActivatedRoute,
    private appService:AppService,
    private router:Router,
    private _bs:BehaviorService,
    private toastr:ToastrService) {
    this.addEditForm=_fb.group({
      type:['spa',Validators.required],
      name:['',Validators.required],
      image:['']
    })
    this.id=this.activeRoute.snapshot.params.id
   }

  ngOnInit(): void {
    if(this.id) this.getdata()
  }

  getdata(){
    this._bs.load(true)
    this.appService.getAll('common-category',{id:this.id}).subscribe(res=>{
      if(res.success){
        this.addEditForm.patchValue(res.data)
      }
      this._bs.load(false)
    },err=>{
      this._bs.load(false)
    })
  }

  updating:any=false
  onSubmit(){
    this.submitted=true
    if(this.addEditForm.valid){
      let url='common-category'
      let method='post'
      let payload:any={
        ...this.addEditForm.value
      }
      if(this.id){
        payload.id=this.id
        method='put'
      } 
      this.updating=true
      this.appService.allApi(url,payload,method).subscribe(res=>{
        if(res.success){
          this.toastr.success(res.message)
          this.router.navigateByUrl('/category')
        }
        this.updating=false
      },err=>{
        this.updating=false
      })
    }
  }

  get f() { return this.addEditForm.controls;}

  imageUploading:any=false
  updateImage(e:any){
    let files=e.target.files
    let fdata={
      modelName:'category',
      file:files.item(0)
    }
    this.imageUploading=true
    this.appService.uploadImage('upload/image?modelName=category',fdata).subscribe(res=>{
      if(res.success){
        let image=res.data.fullpath
        this.addEditForm.patchValue({image})
      }
      this.imageUploading=false
    },err=>{
      this.imageUploading=false
    })
  }
}
