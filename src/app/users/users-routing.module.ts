import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddUpdateComponent } from './add-update/add-update.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';

const routes: Routes = [
  {
    path:'',
    component:ListComponent
  },
  {
    path:'view/:id',
    component:ViewComponent
  },
  {
    path:'add',
    component:AddUpdateComponent
  },
  {
    path:'edit',
    component:AddUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
