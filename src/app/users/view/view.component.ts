import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { BehaviorService } from 'src/app/shared/behavior.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  data:any
  id:any
  host:any=environment.apiUrl
  constructor(private appService:AppService,
    private activeRoute:ActivatedRoute,
    private _bs:BehaviorService) {
      this.id=activeRoute.snapshot.params.id
     }

  ngOnInit(): void {
    this.getdata()
  }

  getdata(){
    this._bs.load(true)
    this.appService.getAll('user/detail',{id:this.id}).subscribe(res=>{
      if(res.success){
        this.data=res.data
      }
      this._bs.load(false)
    },err=>{
      this._bs.load(false)
    })
  }


}
