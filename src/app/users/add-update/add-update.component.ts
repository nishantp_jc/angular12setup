import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppService } from 'src/app/app.service';
import { BehaviorService } from 'src/app/shared/behavior.service';
import sharedModel from 'src/models/shared.model';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-add-update',
  templateUrl: './add-update.component.html',
  styleUrls: ['./add-update.component.scss']
})
export class AddUpdateComponent implements OnInit {

  addEditForm:FormGroup;
  submitted:any = false;
  id:any;
  data:any;
  _host:any=environment.apiUrl
  categories:any
  constructor(private fb:FormBuilder,
    private _activateRouter: ActivatedRoute,
    private appService:AppService,
    private toastr:ToastrService,
    private router:Router,
    private _bs:BehaviorService) {
    this.addEditForm = this.fb.group({
      email: ['', [Validators.email, Validators.pattern("^[a-z0-9._%. +-]+@[a-z0-9.-]+\\.[a-z]{2,4}.$")]],
      fullName: [''],
      image: [''],
      role: ['spa'],
      spa_id:[''],
      address: [''],
      lat: [''],
      lng: [''],
      city: [''],
      country: [''],
      state: [''],
      id:[],
      mobileNo: ['',[Validators.required]]
    });
    
  }

  ngOnInit(): void {
    if(this.getParam('id')) {
      this.id = this.getParam('id')
      this.addEditForm.patchValue({id:this.id})
      this.getData()
    }
    this.getcategories()
  }

  getcategories(){
    this.appService.getAll('common-category/all',{page:1,count:100}).subscribe(res=>{
      if(res.success){
        this.categories=res.data.data
      }
    })
  }

  getParam(p:any) {
    return this._activateRouter.snapshot.queryParamMap.get(p) || ''
  }

  get f() { return this.addEditForm.controls;}


  getData(){
    let filter = {
      id:this.id
    }

    this._bs.load(true)
    this.appService.getAll('user/detail',filter).subscribe((res:any)=>{
      if(res.success){
        let data = res.data;
        data.mobileNo=sharedModel.getTelInputValue(data)
        data.spa_id=data.spa_id.id
        this.addEditForm.patchValue(data)
        if(data.lat) this.isAddressSelected=true
      }

      this._bs.load(false)
    })
  }

  onSubmit(){
    this.submitted = true;
    if (this.addEditForm.invalid || !this.isAddressSelected) return;

   

    let method = 'post';
    let url = 'add/user';
    if(this.id){
      method = 'put';
      url = 'edit/profile'
    }else{
      delete this.addEditForm.value.id
    }
    let value = this.addEditForm.value
    let payload = {
      ...value,
      dialCode: value.mobileNo.dialCode,
      mobileNo: value.mobileNo.number,
      countryCode: value.mobileNo.countryCode,
    }

    this._bs.load(true)
    this.appService.allApi(url,payload, method).subscribe((res)=>{
      if(res.success){
        this.toastr.success(res.message)
        this.router.navigateByUrl('/users')
      }
      this._bs.load(false)
    })
  }

  imageUploading:any=false
  updateImage(e:any){
    let files=e.target.files
    let fdata={
      modelName:'users',
      file:files.item(0)
    }
    this.imageUploading=true
    this.appService.uploadImage('upload/image?modelName=users',fdata).subscribe(res=>{
      if(res.success){
        let image=res.data.fullpath
        this.addEditForm.patchValue({image})
      }
      this.imageUploading=false
    },err=>{
      this.imageUploading=false
    })
  }

  userImg(img:any){
    let value = './assets/img/profile.jpg';

    if(img && img.includes('https://')){
      value = img;
    }
    else if(img){
      value = this._host+'images/users/'+img
    }

    return value;
  }

  isAddressSelected:any=false
  handleAddressChange(address:any) {
    let aaddress=sharedModel.getAddress(address)
    this.isAddressSelected=true
    console.log("aaddress",aaddress)
      this.addEditForm.patchValue(aaddress)

	}

  addressCheck(){
    this.isAddressSelected=false
  }
}
